//
//  ChangeNameVC.h
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeNameDelegate <NSObject>

-(void)canceNameChange;
-(void)saveNameChange:(NSString *)newName;

@end

@interface ChangeNameVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *exisingNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *updatedNameTextField;

@property(strong, nonatomic) id<ChangeNameDelegate> delegate;
@property(strong, nonatomic) NSString *initialName;

@end
