//
//  BeaconDAO.h
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTBeacon.h"

@interface BeaconDAO : NSObject

- (void)saveBeaconData:(ESTBeacon *)beacon;

@end
