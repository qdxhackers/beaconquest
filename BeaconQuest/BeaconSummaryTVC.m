//
//  BeaconSummaryTVC.m
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import "BeaconSummaryTVC.h"

@implementation BeaconSummaryTVC

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBeacon:(ESTBeacon *)beacon {
    _beacon = beacon;
    
    [self updateViews];
}

- (void)updateViews {
    self.nameLabel.text = self.beacon.name;
    self.distanceLabel.text = [NSString stringWithFormat:@"Distance: %.2f m", [self.beacon.distance floatValue]];
    self.majorLabel.text = [NSString stringWithFormat:@"%@", self.beacon.major];
    self.minorLabel.text = [NSString stringWithFormat:@"%@", self.beacon.minor];
}

@end
