//
//  ChangeNameVC.m
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import "ChangeNameVC.h"

@interface ChangeNameVC ()

@end

@implementation ChangeNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Edit Beacon";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(save)];
    
    self.exisingNameLabel.text = self.initialName;
}

-(void)cancel {
    [self.delegate canceNameChange];
}

-(void)save {
    [self.delegate saveNameChange:self.updatedNameTextField.text];
}

@end
