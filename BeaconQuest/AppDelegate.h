//
//  AppDelegate.h
//  BeaconQuest
//
//  Created by Brandon Brock on 11/13/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

