//
//  ViewController.h
//  BeaconQuest
//
//  Created by Brandon Brock on 11/13/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeNameVC.h"

@interface BeaconListVC : UIViewController<UITableViewDelegate, UITableViewDataSource, ChangeNameDelegate>

typedef enum : int
{
    ESTScanTypeBluetooth,
    ESTScanTypeBeacon
    
} ESTScanType;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)savePressed:(UIButton *)sender;

@end

