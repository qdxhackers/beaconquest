//
//  BeaconSummaryTVC.h
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeacon.h"

@interface BeaconSummaryTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *majorLabel;
@property (strong, nonatomic) IBOutlet UILabel *minorLabel;
@property (strong, nonatomic) ESTBeacon *beacon;

@end
