//
//  ViewController.m
//  BeaconQuest
//
//  Created by Brandon Brock on 11/13/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import "BeaconListVC.h"
#import "ESTBeaconManager.h"
#import "BeaconSummaryTVC.h"
#import "ChangeNameVC.h"
#import "BeaconDAO.h"

@interface BeaconListVC () <ESTBeaconManagerDelegate>
@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) ESTBeaconRegion *region;
@property (nonatomic, strong) NSArray *beaconsArray;
@property (nonatomic, assign)   ESTScanType scanType;
@end

@implementation BeaconListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSaveButton];
    
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    
    self.scanType = ESTScanTypeBeacon;

    [ESTBeaconManager setupAppID:@"app_0aakgtxd2h" andAppToken:@"7c033b4c85497c8488b39ad606262109"];
    
    /*
     * Creates sample region object (you can additionaly pass major / minor values).
     *
     * We specify it using only the ESTIMOTE_PROXIMITY_UUID because we want to discover all
     * hardware beacons with Estimote's proximty UUID.
     */
    self.region = [[ESTBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID
                                                      identifier:@"EstimoteSampleRegion"];
    
    /*
     * Starts looking for Estimote beacons.
     * All callbacks will be delivered to beaconManager delegate.
     */
    if (self.scanType == ESTScanTypeBeacon)
    {
        [self startRangingBeacons];
    }
    else
    {
        [self.beaconManager startEstimoteBeaconsDiscoveryForRegion:self.region];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    /*
     *Stops ranging after exiting the view.
     */
    [self.beaconManager stopRangingBeaconsInRegion:self.region];
    [self.beaconManager stopEstimoteBeaconDiscovery];
}

- (void)setupSaveButton {
    UIColor *color = [UIColor colorWithRed:58.0/255.0 green:192.0/255.0 blue:61.0/255.0 alpha:1.0];
    
    self.saveButton.layer.cornerRadius = 4;
    self.saveButton.layer.borderWidth = 1;
    self.saveButton.layer.borderColor = color.CGColor;
    [self.saveButton setTitleColor:color forState:UIControlStateNormal];
}

#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    self.beaconsArray = beacons;
    
    [self.tableView reloadData];
}

- (void)beaconManager:(ESTBeaconManager *)manager didDiscoverBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    self.beaconsArray = beacons;
    
    [self.tableView reloadData];
}

#pragma mark - Beacon Ranging

-(void)startRangingBeacons
{
    if ([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        /*
         * Request permission to use Location Services. (new in iOS 8)
         * We ask for "always" authorization so that the Notification Demo can benefit as well.
         * Also requires NSLocationAlwaysUsageDescription in Info.plist file.
         *
         * For more details about the new Location Services authorization model refer to:
         * https://community.estimote.com/hc/en-us/articles/203393036-Estimote-SDK-and-iOS-8-Location-Services
         */
        [self.beaconManager requestAlwaysAuthorization];
    }
    else if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.beaconManager startRangingBeaconsInRegion:self.region];
    }
    else if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Access Denied"
                                                        message:@"You have denied access to location services. Change this in app settings."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
    }
    else if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Not Available"
                                                        message:@"You have no access to location services."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
    }
}

#pragma mark - TableView Delegates

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Beacons in range";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.beaconsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BeaconSummaryTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"BeaconSummaryCell"];

    ESTBeacon *beacon = [self.beaconsArray objectAtIndex:indexPath.row];
    cell.beacon = beacon;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *storyboard =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangeNameVC *changeNameVC = (ChangeNameVC *)[storyboard instantiateViewControllerWithIdentifier:@"ChangeNameVC"];
    changeNameVC.delegate = self;
    changeNameVC.initialName = ((ESTBeacon *)[self.beaconsArray objectAtIndex:indexPath.row]).name;

    UINavigationController *navVC = [[UINavigationController  alloc] initWithRootViewController:changeNameVC];
    [self presentViewController:navVC animated:YES completion:nil ];
}

#pragma mark - Change Name Delegates

-(void)canceNameChange {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveNameChange:(NSString *)newName {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //TODO - save name
}

#pragma mark - DAO Call


- (IBAction)savePressed:(UIButton *)sender {
    BeaconDAO *dao = [[BeaconDAO alloc] init];
    ESTBeacon *beacon = [self.beaconsArray objectAtIndex:0];
    [dao saveBeaconData:beacon];
}
@end
