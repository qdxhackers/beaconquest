//
//  BeaconDAO.m
//  BeaconQuest
//
//  Created by Brandon Brock on 11/14/14.
//  Copyright (c) 2014 Brandon Brock. All rights reserved.
//

#import "BeaconDAO.h"
#import "AFHTTPRequestOperationManager.h"

@implementation BeaconDAO

-(void)saveBeaconData:(ESTBeacon *)beacon {
    
    // user as the parent level
    NSDictionary *params = @ {@"name" :@"brandon brock", @"email" :@"bb44wsu@gmail.com", @"phone" :@"51387678416" };

    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [operationManager POST:@"http://192.168.43.158:8080/iBeaconRest/rest/ibs/process"
                parameters:params
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       NSLog(@"JSON: %@", [responseObject description]);
                   }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       NSLog(@"Error: %@", [error description]);
                   }
     ];
}

@end
